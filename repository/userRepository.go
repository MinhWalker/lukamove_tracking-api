package repository

type User struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Location string `json:"location"`
}

func (user *User) GetId() string {
	return user.Id
}

func (user *User) GetName() string {
	return user.Name
}

func (user *User) GetLocation() string {
	return user.Location
}



