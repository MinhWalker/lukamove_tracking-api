module LukaMove_tracking-api

go 1.16

require (
	github.com/go-redis/redis/v8 v8.10.0 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
)
