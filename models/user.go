package models

type User interface {
	GetId() string
	GetName() string
	GetLocation() string
}

