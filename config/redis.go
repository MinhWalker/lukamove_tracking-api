package config

import "github.com/go-redis/redis/v8"

var Redis *redis.Client

func CreateRedisClient() {
	opt, err := redis.ParseURL("redis://host.docker.internal:6379/0")	//host.docker.internal
	if err != nil {
		panic(err)
	}

	redis := redis.NewClient(opt)
	Redis = redis
}

